#!/usr/bin/env sh
set -e pipefail

env

npm run $DOCKER_STAGE

exec "$@"

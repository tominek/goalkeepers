import { agent as request } from 'supertest'

import app from '../../src/app'

describe('Health check', () => {
  describe('GET /health', () => {
    it('should return 200 OK', async () => {
      await request(app)
        .get('/health')
        .expect(200)
    })
  })
})

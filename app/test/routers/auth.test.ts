import { agent as request } from 'supertest'
import { internet, name } from 'faker'
import { expect } from 'chai'
import { omit } from 'lodash'

import app from '../../src/app'
import { registerUser } from '../../src/services/user.service'

interface TestUser {
  id: number
  firstname: string,
  lastname: string,
  email: string,
  password: string,
  rawPassword: string
}

async function registerTestUser (): Promise<TestUser> {
  const rawPassword = internet.password(8)
  const user = await registerUser({
    firstname: name.firstName(),
    lastname: name.lastName(),
    email: internet.email().toLowerCase(),
    password: rawPassword,
  })

  return {
    ...user,
    rawPassword,
  }
}

describe('Auth router', () => {
  describe('POST /auth/register', () => {
    it('should register new user', async () => {
      const user = {
        firstname: name.firstName(),
        lastname: name.lastName(),
        password: internet.password(8),
        email: internet.email(),
      }

      await request(app)
        .post('/auth/register')
        .send(user)
        .expect(201)
        .then(res => {
          // console.log(res.body)
          expect(res.body).to.have.property('firstname', user.firstname)
          expect(res.body).to.have.property('lastname', user.lastname)
          expect(res.body).to.have.property('email', user.email.toLowerCase())
          expect(res.body).to.not.have.property('password')
        })
    })
    it('should not register user with duplicate email', async () => {
      const user = await registerTestUser()

      await request(app)
        .post('/auth/register')
        .send(user)
        .expect(400)
    })

    const user = {
      firstname: name.firstName(),
      lastname: name.lastName(),
      password: internet.password(8),
      email: internet.email().toLowerCase(),
    }
    let examples = [
      {
        attribute: 'firstname',
        data: omit(user, ['firstname']),
      },
      {
        attribute: 'lastname',
        data: omit(user, ['lastname']),
      },
      {
        attribute: 'email',
        data: omit(user, ['email']),
      },
      {
        attribute: 'password',
        data: omit(user, ['password']),
      },
    ]
    for (const example of examples) {
      it(`should return bad request when mandatory attribute missing - ${example.attribute}`, async () => {
        await request(app)
          .post('/auth/register')
          .send(example.data)
          .expect(400)
      })
    }

  })
  describe('POST /auth/login', () => {
    it('should return access token', async () => {
      const user = await registerTestUser()

      await request(app)
        .post('/auth/login')
        .send({
          email: user.email,
          password: user.rawPassword,
        })
        .expect(200)
        .then(res => {
          // console.log(res.body)
          expect(res.body).to.have.property('accessToken')
        })
    })
    it('should return unauthorized when unknown email', async () => {
      await request(app)
        .post('/auth/login')
        .send({
          email: internet.email().toLowerCase(),
          password: internet.password(8),
        })
        .expect(401)
    })
    it('should return unauthorized when invalid password', async () => {
      const user = await registerTestUser()

      await request(app)
        .post('/auth/login')
        .send({
          email: user.email,
          password: 'invalid password',
        })
        .expect(401)
    })

    const credentials = {
      email: internet.email().toLowerCase(),
      password: internet.password(8),
    }
    let examples = [
      {
        attribute: 'email',
        data: omit(credentials, ['email']),
      },
      {
        attribute: 'password',
        data: omit(credentials, ['password']),
      },
    ]
    for (const example of examples) {
      it(`should return bad request when mandatory attribute missing - ${example.attribute}`, async () => {
        await request(app)
          .post('/auth/login')
          .send(example.data)
          .expect(400)
      })
    }

  })
})

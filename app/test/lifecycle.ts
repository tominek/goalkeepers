import { initialize } from 'objection'
import { Goalkeeper } from '../src/models/goalkeeper'
import { User } from '../src/models/user'
import { knex } from '../src/app'

// Before running any tests...
before(async function () {
  this.timeout(10000)
  await knex.migrate.latest()
  await initialize([Goalkeeper, User])
})

beforeEach(async () => {
// await knex.schema.dropTable('user') TODO
// await knex.schema.dropTable('goalkeeper')
})

// afterEach(async () => {})

// after(async () => {})

import { initialize } from 'objection'
import { Goalkeeper } from './src/models/goalkeeper'
import { User } from './src/models/user'
import app from './src/app'

const PORT = process.env.PORT ?? 80

app.listen(PORT, async () => {
  await initialize([Goalkeeper, User])
  console.info(`🚀 Server ready at http://localhost:${PORT}`)
})

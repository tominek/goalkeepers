import * as Knex from 'knex'

export async function up (knex: Knex): Promise<void> {
  return knex.schema
    .createTable('user', table => {
      table.increments('id').primary()

      table.string('firstname')
      table.string('lastname')
      table.string('email')
      table.string('password')
    })
    .createTable('goalkeeper', table => {
      table.increments('id').primary()

      table
        .integer('owner')
        .unsigned()
        .references('id')
        .inTable('user')
        .onDelete('CASCADE')
        .index()

      table.string('firstname')
      table.string('lastname')
      table.integer('rating')
      table.boolean('public')
    })
}


export async function down (knex: Knex): Promise<void> {
  return knex.schema
    .dropTableIfExists('user')
    .dropTableIfExists('goalkeeper')
}


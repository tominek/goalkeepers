import express from 'express'
import bodyParser from 'body-parser'
import { Model } from 'objection'
import Knex from 'knex'

import authRouter from './routers/auth'
import gRouter from './routers/goalkeepers'
import config from '../knexfile'

const NODE_ENV = process.env.NODE_ENV as string

const app = express()

app.use(bodyParser.json())

const knex = Knex(NODE_ENV === 'test' ? config.test : config.production)
Model.knex(knex)

if (NODE_ENV === 'development') {
  app.use((req, res, next) => {
    console.debug(`${req.method} ${req.url}`, { body: req.body })
    next()
  })
}

app.get('/health', (req, res) => {
  res.sendStatus(200)
})

app.use('/auth', authRouter)
app.use('/g', gRouter)

app.use('*', (req, res) => {
  res.sendStatus(404)
})

export default app

export {
  knex,
}

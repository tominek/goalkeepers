import { cloneDeep } from 'lodash'

/**
 * Creates new object while deleting all undefined fields.
 */
export function cleanUndefined (obj: Record<string, any>): Record<string, any> {
  const clean = cloneDeep(obj)
  Object.keys(clean).forEach((key) => (clean[key] === undefined ? delete clean[key] : {})) // eslint-disable-line

  return clean
}

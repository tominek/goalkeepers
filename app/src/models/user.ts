import { Model } from 'objection'

import { Goalkeeper } from './goalkeeper'

export class User extends Model {
  static tableName = 'user'
  static idColumn = 'id'
  static jsonSchema = {
    type: 'object',
    required: ['firstname', 'lastname', 'email', 'password'],

    properties: {
      id: { type: 'integer' },
      firstname: { type: 'string', minLength: 1, maxLength: 255 },
      lastname: { type: 'string', minLength: 1, maxLength: 255 },
      email: { type: 'string', minLength: 1, maxLength: 255 },
      password: { type: 'string', min: 0, max: 5 },
      goalkeepers: { type: 'array' },
    },
  }
  static relationMappings = {
    goalkeepers: {
      relation: Model.HasManyRelation,
      modelClass: Goalkeeper,
      join: {
        from: 'user.id',
        to: 'goalkeeper.owner',
      },
    },
  }

  id!: number
  firstname!: string
  lastname!: string
  email!: string
  password!: string
  goalkeepers!: Goalkeeper[]

  fullName = () => {
    return this.firstname + ' ' + this.lastname
  }
}

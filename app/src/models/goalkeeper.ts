import { Model } from 'objection'

import { User } from './user'

export class Goalkeeper extends Model {
  static tableName = 'goalkeeper'
  static idColumn = 'id'
  static jsonSchema = {
    type: 'object',
    required: ['firstname', 'lastname'],

    properties: {
      id: { type: 'integer' },
      owner: { type: 'integer' },
      firstname: { type: 'string', minLength: 1, maxLength: 255 },
      lastname: { type: 'string', minLength: 1, maxLength: 255 },
      rating: { type: 'number', min: 0, max: 5 },
      shared: { type: 'boolean' },
    },
  }
  static relationMappings = {
    owner: {
      relation: Model.BelongsToOneRelation,
      modelClass: User,
      join: {
        from: 'goalkeeper.owner',
        to: 'user.id',
      },
    },
  }
  id!: number
  owner!: User
  firstname!: string
  lastname!: string
  rating!: number
  shared!: boolean

  fullName () {
    return this.firstname + ' ' + this.lastname
  }
}

import { User } from '../models/user'
import { hashSync } from 'bcrypt'

export interface RegisterUserArgs {
  firstname: string
  lastname: string
  email: string
  password: string
}

export const getById = async (id: number): Promise<User | null> => {
  return User.query().findById(id)
}

export const getUserByEmail = async (email: string): Promise<User | null> => {
  return User.query().findOne({ email: email.toLowerCase() })
}

export const registerUser = async (
  {
    firstname,
    lastname,
    email,
    password,
  }: RegisterUserArgs,
): Promise<User> => {
  return User.query().insert({
    firstname, lastname,
    email: email.toLowerCase(),
    password: hashSync(password, 10),
  })
}

import express, { Request, Response } from 'express'
import { compareSync } from 'bcrypt'
import { sign } from 'jsonwebtoken'
import { pick } from 'lodash'

import { getUserByEmail, registerUser, RegisterUserArgs } from '../services/user.service'

interface LoginArgs {
  email: string
  password: string
}

export interface TokenPayload {
  email: string
}

const JWT_TOKEN_SECRET = process.env.JWT_TOKEN_SECRET as string
const JWT_TOKEN_ISSUER = process.env.JWT_TOKEN_ISSUER as string
const ACCESS_TOKEN_EXPIRATION = process.env.ACCESS_TOKEN_EXPIRATION as string

const authRouter = express.Router()

authRouter.post(
  '/register',
  checkMandatoryRegisterAttributes,
  checkEmailDuplicity,
  async (req: Request<any, any, RegisterUserArgs>, res) => {
    const user = await registerUser(req.body)

    res.status(201)
      .json(pick(user, ['firstname', 'lastname', 'email']))
  },
)

authRouter.post(
  '/login',
  checkRequiredLoginAttributes,
  async (req: Request<any, any, LoginArgs>, res) => {
    const { email, password } = req.body
    const user = await getUserByEmail(email)

    if (!user || !compareSync(password, user.password)) {
      res.sendStatus(401)
    }

    const payload: TokenPayload = { email }

    res.status(200)
      .json({
        accessToken: sign(payload, JWT_TOKEN_SECRET, {
          expiresIn: ACCESS_TOKEN_EXPIRATION,
          issuer: JWT_TOKEN_ISSUER,
        }),
      })
  },
)

export default authRouter

function checkMandatoryRegisterAttributes (req: Request, res: Response, next: Function) {
  let mandatory = [
    'firstname',
    'lastname',
    'email',
    'password',
  ]
  for (const attribute of mandatory) {
    if (!req.body.hasOwnProperty(attribute)) {
      return res.status(400)
        .json({ message: `Request body is missing mandatory attribute '${attribute}'` })
    }
  }
  next()
}

async function checkEmailDuplicity (req: Request<any, any, RegisterUserArgs>, res: Response, next: Function) {
  const { email } = req.body

  const user = await getUserByEmail(email)
  if (user) {
    res.status(400)
      .json({ message: `User with email '${email}' already exists.` })
    return
  }
  next()
}

function checkRequiredLoginAttributes (req: Request, res: Response, next: Function) {
  let mandatory = [
    'email',
    'password',
  ]
  for (const attribute of mandatory) {
    if (!req.body.hasOwnProperty(attribute)) {
      return res.status(400)
        .json({ message: `Request body is missing mandatory attribute '${attribute}'` })
    }
  }
  next()
}

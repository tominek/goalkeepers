import express, { Request, Response } from 'express'
import { verify } from 'jsonwebtoken'

import { TokenPayload } from './auth'
import { User } from '../models/user'
import { Goalkeeper } from '../models/goalkeeper'
import { cleanUndefined } from '../helpers'

interface CreateGoalkeeperArgs {
  firstname: string
  lastname: string
  rating?: number
  shared?: boolean
}

interface UpdateGoalkeeperArgs {
  firstname?: string
  lastname?: string
  rating?: number
  shared?: boolean
}

const JWT_TOKEN_SECRET = process.env.JWT_TOKEN_SECRET as string

const gRouter = express.Router()

gRouter.use(async (req, res, next) => {
  const token: string = req.headers.authorization ?? ''
  try {
    const { email } = verify(token, JWT_TOKEN_SECRET) as TokenPayload
    // @ts-ignore
    req.user = await User.query().findOne({ email })
  } catch (error) {
    res.sendStatus(401)
  }

  next()
})

gRouter.get(
  '',
  async (req, res) => {
    // @ts-ignore
    const user: User = req.user

    res.json(
      await Goalkeeper.query()
        .select('*')
        .where({
          owner: user.id,
        }),
    )
  },
)

gRouter.get(
  '/:id',
  async (req, res) => {
    // @ts-ignore
    const user: User = req.user

    const goalkeeper = await Goalkeeper.query().findById(req.params.id)
    console.log('goalkeeper', goalkeeper)
    if (goalkeeper) {
      if (user === goalkeeper.owner || goalkeeper.shared) {
        res.json(goalkeeper)
        return
      }
    }

    res.sendStatus(404)
  },
)

gRouter.post(
  '',
  (req, res, next) => {
    let mandatory = [
      'firstname',
      'lastname',
    ]
    for (const attribute of mandatory) {
      if (!req.body.hasOwnProperty(attribute)) {
        return res.status(400).json({ message: `Request body is missing mandatory attribute '${attribute}'` })
      }
    }
    next()
  },
  async (req: Request<any, any, CreateGoalkeeperArgs>, res) => {
    // @ts-ignore
    const owner: User = req.user

    const { firstname, lastname, rating = 0, shared = false } = req.body
    const goalkeeper = await Goalkeeper.query()
      .insert({
        owner, firstname, lastname, rating, shared,
      })
    console.log(goalkeeper)

    res.status(201).json(goalkeeper)
  },
)

gRouter.patch(
  '/:id',
  async (req: Request<any, any, UpdateGoalkeeperArgs>, res: Response<Goalkeeper>) => {
    const { firstname, lastname, rating, shared } = req.body

    const goalkeeper = await Goalkeeper.query()
      .updateAndFetchById(req.params.id, cleanUndefined({ firstname, lastname, rating, shared }))

    res.json(goalkeeper)
  },
)

export default gRouter

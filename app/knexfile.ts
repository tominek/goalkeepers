import { Config } from 'knex'

const POSTGRES_URL = process.env.POSTGRES_URL
const POSTGRES_PASSWORD = process.env.POSTGRES_PASSWORD

type KnexEnv = 'test' | 'production'

const config: Record<KnexEnv, Config> = {
  test: {
    client: 'pg',
    connection: {
      host: POSTGRES_URL,
      user: 'postgres',
      password: POSTGRES_PASSWORD,
      database: 'test',
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'migrations',
    },
  },
  production: {
    client: 'pg',
    connection: {
      host: POSTGRES_URL,
      user: 'postgres',
      password: POSTGRES_PASSWORD,
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'migrations',
    },
  },
}

export default config

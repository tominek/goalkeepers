ARG NODE_IMAGE=node:alpine

FROM $NODE_IMAGE as build
WORKDIR /app
COPY ./app .
RUN npm install
RUN npm run build

#FROM $NODE_IMAGE as prod
#ENV DOCKER_STAGE=prod
#COPY .docker/entrypoint.sh /usr/local/bin/
#RUN chmod +x /usr/local/bin/entrypoint.sh
#RUN apk update \
#  && apk --update add python py-pip \
#  && apk --update add --virtual build-dependencies python-dev build-base \
#  && rm -rf /var/cache/apk/*
#WORKDIR /app
#COPY --from=build /app/dist ./dist
#COPY --from=build /app/package*.json ./
#RUN npm install --only=prod
#ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
#EXPOSE 80

FROM $NODE_IMAGE as dev
ENV DOCKER_STAGE=dev
COPY .docker/entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/entrypoint.sh
RUN apk update \
  && apk --update add python py-pip \
  && apk --update add --virtual build-dependencies python-dev build-base \
  && rm -rf /var/cache/apk/*

WORKDIR /app
COPY ./app .
RUN npm install
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
EXPOSE 80

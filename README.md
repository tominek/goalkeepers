# Goalkeepers
> Simple goalkeepers collection management

## How to run in Docker

1. Add record `127.0.0.1   goalkeepers.local` to your hosts file
2. Create a copy of `.env.dist` and `docker-compose.yml.dist` without `dist` suffix
3. Run `docker-compose up -d` in repository root
4. Execute command `npm run migrate` in **goalkeepers_api** container
5. You're done. You should be able to get `200 OK` when you try to access http://goalkeepers.local/health

**Tip:** Comment out **prod** stage in `Dockerfile` if you want to make your docker builds faster during development

## How to test

When you have your docker environment running:

1. Create new database in your Postgres instance named `test`
2. Execute command `npm run test:migrate` in **goalkeepers_api** container
2. Execute command `npm run test` in **goalkeepers_api** container

**Tip:** Database is accessible on `localhost:5432`
